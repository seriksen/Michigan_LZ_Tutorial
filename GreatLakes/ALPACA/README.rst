ALPACA
~~~~~~

ALPACA is a software package that allows the user to process, alter, analyze, and produce ROOT files.
There are many useful tools and extensions to ALPACA which can be read in more detail in the
`ALPACA software docs <https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/intro.html>`_.
Because CVMFS is not current (May 2021) setup on the Great Lakes, the setup differs from the LZ software docs.

In order to get ALPACA running, perform the following steps;

.. code:: sh

    # Make a directory and git-clone ALPACA
    mkdir ALPACA && cd ALPACA
    git clone git@gitlab.com:luxzeplin/analysis/alpaca/alpaca.git .

    # Set the version of ALPACA you want to run
    ALPACA_VERSION=1.9.0

    # Copy the ALPACA patch for the Great Lakes
    cp /path/to/ALPACA_${ALPACA_VERSION}_GL.patch .

    # Checkout the version of ALPACA needed
    git checkout tags/${ALPACA_VERSION}

    # Apply the patch
    git apply ALPACA_${ALPACA_VERSION}_GL.patch

    # Now source and build in the normal way (as described in the LZ software docs)
    source setup.sh
    source build.sh


.. note::
    The :code:`ALPACA_${ALPACA_VERSION}_GL.patch` is included in the Michigan_LZ_Tutorial under the GreatLakes directory.
    It can also be found at this address;
    `ALPACA_GreatLakes.patch <https://gitlab.com/seriksen/Michigan_LZ_Tutorial/-/blob/master/GreatLakes/>`_