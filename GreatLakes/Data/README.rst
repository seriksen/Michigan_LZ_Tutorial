Getting data onto the Great Lakes
---------------------------------

LZ data (simulated and real) is saved at NERSC.
In order to use it on the Great Lakes it needs to be copied across.
As we typically want to copy TB of data at a time, using :code:`scp` isn't practical.
Instead, we use `globus <https://www.globus.org/>`_.

In order to copy data from NERSC to the Great Lakes, you need to know the EndPoints to use,
in other words - 'where does the data start and where does it go'.
You can get the names of these for NERSC and the GreatLakes at the links below;

1. `NERSC Endpoints <https://docs.nersc.gov/services/globus/>`_
2. `Great Lakes Endpoints <https://arc.umich.edu/document/file-transfers-with-globis-gridftp/>`_

Typically I use :code:`NERSC DTN` to :code:`umich#greatlakes`.

Documentation on Globus can be found `here <https://docs.globus.org/how-to/get-started/>`_.

.. note::
    We do not have unlimited storage at the greatlakes, therefore, before you copy data across, contact
    Sam to check that there is sufficient space and if-not can request more.


.. todo::
    Add slides from John on globus

