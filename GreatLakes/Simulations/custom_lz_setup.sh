#!/bin/bash
#######################
##
## Name: custom_lz_setup
##
## Author: Sam Eriksen
##
## Description: Bash script to access BACCARAT on the Great Lakes
##
## Usage: `source custom_lz_setup.sh`
##
#######################
#
# Base Location
michigan_base=/nfs/turbo/lsa-penningb/software #/sam
export michigan_base
#
# Define variables to pass to LZBuild
LZBUILD_DIR=${michigan_base}/cvmfs/lz.opensciencegrid.org/LzBuild/release-3.0.2
export LZBUILD_DIR
LZ_EXTERNAL_DIR=${michigan_base}/cvmfs/lz.opensciencegrid.org/external
export LZ_EXTERNAL_DIR
LZ_BASE_DIR=${michigan_base}/cvmfs/lz.opensciencegrid.org
export LZ_BASE_DIR
#
# GCC
module load gcc/8.2.0
#
# Source BACCARAT
source ${michigan_base}/cvmfs/sam/lz.opensciencegrid.org/BACCARAT/release-6.1.6/x86_64-centos7-gcc8-opt/setup.sh
#
# Add libraries required which would normally be on cvmfs
# Xerces, Armadillo, Boost, MLPack
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${michigan_base}${XERCESCROOT}/lib64:${ARMADILLO_ROOT_DIR}/lib64:${michigan_base}${BOOST_ROOT_DIR}/lib:${MLPACK_ROOT_DIR}/lib64
# LZFieldMaps, Yaml, DmCalc
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${LZFieldMaps_DIR}/../:${yaml_cpp_DIR}/lib:${DMCalc_ROOT_DIR}/lib
# qhull
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${michigan_base}/cvmfs/lz.opensciencegrid.org/external/qhull/7.3.2/x86_64-centos7-gcc8-opt/lib
# GSL
module load gsl/2.5
export LD_LIBRARY_PATH
#
# If you are seeing errors, turn on debugging
export LZ_DEBUG=1
#
