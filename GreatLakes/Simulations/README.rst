Simulations
~~~~~~~~~~~

This differs from running at NERSC as no shifter image is required.
However, currently we are not able to mount cmvfs so we have to have a work around.
Rather than running the cvmfs setup script, we run a custom one.
The custom setup script is shown in the GreatLakes directory.

Navigate into that directory and run the following;

.. code-block:: sh

   source custom_lz_setup.sh

Now you will have access the :code:`BACCARATExecutable` and can move onto the :doc:`baccarat_tutorial`.
