Great Lakes Job Submission
==========================

Eventually you'll want to run batch jobs (ie rather than running on a login node, send them to a compute node).
On the Great Lakes this process is controlled by `slurm <https://slurm.schedmd.com/overview.html>`_.

The `Great Lakes manuals <https://arc.umich.edu/greatlakes/slurm-user-guide/>`_ have more information on how to actually
do this as well as good examples, but below I've highlighted what are likely to be the ways you want to use it.

Before starting there are a few things to mention about compute nodes vs login nodes.

- When you log into a remote machine, you are doing this on a shared login node. These are not very powerful and used by many people. On clusters (such as the great lakes), they are designed to run very simple task such as submitting jobs to the actual compute nodes. However, as they are a communal resource, they are 'free' to use.
- The compute nodes are much newer and more powerful. They are designed to do heavy workloads for long hours. However, they cannot be ssh'd onto like the login nodes. This is because each CPU-core of the compute node is requested a reserved by a user for their computing needs until their process (computing jobs) is complete. Because compute nodes are exclusive they are 'charged per CPU per time' they are used.
- Some precautions do need to be taken before running things on a compute node vs a login node, with the following questions needing answering;
    1. How many CPUs do you need?
    2. How much memory (RAM) is needed per CPU?
    3. How are you splitting up your job?
    4. How long will your jobs take?

Direct CMD Submission
~~~~~~~~~~~~~~~~~~~~~
The most basic usage of slurm is via the command line.
This is what you will find in most tutorials.

Essentially this is a standard bash script but it contains some slurm specific lines at the top.
For example, the following would run 10 instances of the s2photonbomb from the BACCARAT tutorial.
:code:`sbatch run_job.sh`, where :code:`run_job.sh` is;

.. code-block:: sh

    #!/bin/bash
    #SBATCH --job-name=ten_sims
    #SBATCH --mail-user=uniqname@umich.edu
    #SBATCH --mail-type=BEGIN,END
    #SBATCH --ntasks=10
    #SBATCH --cpus-per-task=1
    #SBATCH --time=1:00:00
    #SBATCH --nodes=1
    #SBATCH --mem-per-cpu=2GB
    #SBATCH --account=penningb1
    #SBATCH --partition=standard
    #SBATCH --output=/home/%u/%x-%j.log
    #Run Simulation
    source ${HOME}/Michigan_LZ_Tutorial/GreatLakes/custom_lz_setup.sh
    macro=${HOME}/Michigan_LZ_Tutorial/macros/s2photonbomb.mac
    export OUTDIR=${HOME}/Michigan_LZ_Tutorial/GreatLakes/bacc_outdir
    export NEVENTS=10000
    export SEED=$SLURM_JOB_ID
    BACCARATExecutable ${macro}

Notice that lines begining with :code:`#SBATCH` are the slurm commands.
I am telling slurm to submit;

- 10 jobs (tasks)
- Each jobs needs 1 CPU (as BACCARAT is a single core process)
- Each jobs (so BACCARAT run) needs 2GB of RAM
- It will be billed to `penningb1`
- If the job doesn't finish in 1-hour then kill it (This stops jobs being charged if there is a problem. Jobs will be killed after 30-days if this isn't set)
- Run on a standard node (other options include a large memory node and GPUs)
- Save the output and errors to log files under :code:`/home/username/jobsid-jobname.log`


What will actually happen?
Slurm will run the following 10-times.

.. code-block:: sh

    source ${HOME}/Michigan_LZ_Tutorial/GreatLakes/custom_lz_setup.sh
    macro=${HOME}/Michigan_LZ_Tutorial/macros/s2photonbomb.mac
    export OUTDIR=${HOME}/Michigan_LZ_Tutorial/GreatLakes/bacc_outdir
    export NEVENTS=10000
    export SEED=$SLURM_JOB_ID
    BACCARATExecutable ${macro}


Which nodes and CPUs are used is determined by the batch submission manager (ie slurm).

Note also that I am setting the random seed for BACCARAT via the :code:`$SLURM_JOB_ID`.
This is a variable set by slurm when it starts the job.
There are other environmental variables you can use - for example if you use job arrays rather than tasks then you
would use :code:`$SLURM_ARRAY_TASK_ID`.

Ganga
-----
Ganga is a python batch submission handler which I use extensively.
It's a wrapper for every type of batch system and local job splitting.
I would recommend this above all others.

A simple ganga script can be run by :code:`ganga submission_script.py`, where :code:`submission_script.py` is;

.. code-block:: python

    # Start ganga job
    j = Job()
    j.application = Executable()
    j.application.exe = File('run_sim.sh')
    j.splitter = GenericSplitter()
    j.splitter.attribute = 'application.env'
    j.splitter.values = splitter_env_vals
    j.backend = Slurm()
    j.submit()

where :code:`run_sim.sh` is;

.. code-block:: sh

    source ${HOME}/Michigan_LZ_Tutorial/GreatLakes/custom_lz_setup.sh
    macro=${HOME}/Michigan_LZ_Tutorial/macros/s2photonbomb.mac
    export OUTDIR=${HOME}/Michigan_LZ_Tutorial/GreatLakes/bacc_outdir
    export NEVENTS=10000
    export SEED=$SLURM_JOB_ID
    BACCARATExecutable ${macro}

This approach has the benefit of, with 1-line change (:code:`j.backend = Slurm()`), the batch manager can be changed.


Interactive Compute Nodes
~~~~~~~~~~~~~~~~~~~~~~~~~
Another way of running on a compute node on the GL is interactively.
This can broadly be done in 1 of 2 ways.

.. note:: If running interactively, ensure that you aren't just running a compute node with nothing processing! Kill the instance if you aren't running things!

Desktops
--------
You can boot up a desktop taking up - up to a full compute node.
This may be most comfortable to begin with as you get a full desktop experience and a terminal so things can be
submitted in a similar way to the login node (ie you run jobs interactively).

A key benefit of this submission approach is that for running ALPACA, the standard job splitter will work fine.

Jupyter Lab
-----------
`Linked here <https://gitlab.com/seriksen/Michigan_LZ_Tutorial/-/blob/master/GreatLakes/Batch_Jobs/JupyterLab%20on%20GL.pdf>`_
is a pdf walk-through on how to set up a jupyter lab instance.
If planning on using the interactive approach, the walk-through will help with both Desktop and jupyter-lab.


