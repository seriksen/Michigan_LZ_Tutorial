BACCARAT Tutorial
=================

Once you are setup on a system and have sources the setup script, you will be able to run BACCARAT.
If you are on cori, you should use the jupyter notebook to the run code the first time, and then come back here to
see how to perform the actions on the command line. The notebook contains the same instructions as here but is
integrated with the code.

There are 3 things that you need to have done before these tutorials will work;

1. In order to run any of the tutorials here, you will need to :code:`git clone` the
`Michigan LZ Tutorial <https://gitlab.com/seriksen/Michigan_LZ_Tutorial>`_ code here.
2. You have access to the LZ software (sourced from cvmfs or a custom setup script).
You can find instructions on the Great Lakes and the Cori pages.
3. You have created a conda environment from :code:`environment.yml`.

.. note::
   After running this tutorial, you should look at the
   `gitlab BACCARAT tutorial <https://luxzeplin.gitlab.io/docs/softwaredocs/simulations/baccarat/tutorials/Tuto1.html>`_,
   however as it requires knowledge of C++ and is less readable it's recommended that you start with this as it's python
   only.

Tutorial 1: S2 photons
~~~~~~~~~~~~~~~~~~~~~~

The simplest simulation which can be performed is an S2 photon bomb.
This is a scintillation light produced when electrons pass through the gaseous Xenon.
It is the most simple because no other interactions happen; the photons are produced, they bounce around the detector
until they are absorbed (either by PMTs - resulting in a detection - or, by another surface).

Be sure to look at the macro `macros/s2photonbomb.mac` as it contains information about what we are trying to run here.

To run BACCARAT, first navigate into the directory corresponding to system you are running on
:code:`cori` or :code:`greatlakes`, and then type the following on the command line;

.. code-block:: sh

   # macro defining the simulation parameters for BACCARAT
   macro=../macros/s2photonbomb.mac
   export OUTDIR=bacc_outdir
   export NEVENTS=10000
   export SEED=42

   # make a directory for the BACCARAT output
   mkdir -p bacc_outdir

   # Run the simulation
   BACCARATExecutable ${macro}

You have now run BACCARAT for the first time.
This has simulated 10,000 optical photons and recorded where they
have gone in the detector.
However, it isn't very useful at this stage.
In order to be able to use it we need to convert it into a ROOT file.
You can do this by the following;

.. code-block:: sh

   # Run the converter to produce a ROOT file
   BaccRootConverter ${OUTDIR}/s2photonbomb_${SEED}.bin

You will now have a file called :code:`s2photonbomb_42.root`.
It contains information about the energies, directions and interactions
that the photons had.

Now lets try and do something with it...

Explore the simulation
----------------------
Enter the conda environment by :code:`conda activate michigan_tutorial` and start a python instance.
In the following example, I am in the :code:`Michigan_LZ_Tutorial` directory, but you can be in any directory if you
add this location to your :code:`PYTHONPATH`.


Lets look at what's in the ROOT file;

.. code-block:: python

    import lz_tutorial
    lz_tutorial.baccarat_tutorial_1.view_baccarat_contents("GreatLakes/bacc_outdir/s2photonbomb_42.root")

It will output something similar to this;

.. code-block:: sh

    TClass::Init:0: RuntimeWarning: no dictionary for class BaccRootConverterEvent is available
    TClass::Init:0: RuntimeWarning: no dictionary for class volumeInfo is available
    TClass::Init:0: RuntimeWarning: no dictionary for class primaryParticleInfo is available
    TClass::Init:0: RuntimeWarning: no dictionary for class trackInfo is available
    TClass::Init:0: RuntimeWarning: no dictionary for class stepInfo is available
    TFile**		GreatLakes/bacc_outdir/s2photonbomb_42.root
     TFile*		GreatLakes/bacc_outdir/s2photonbomb_42.root
      KEY: TTree	HeaderTree;1	Header tree
      KEY: TTree	DataTree;1	Data tree
    ******************************************************************************
    *Tree    :DataTree  : Data tree                                              *
    *Entries :    10000 : Total =         5661321 bytes  File  Size =    1875570 *
    *        :          : Tree compression factor =   3.02                       *
    ******************************************************************************
    *Branch  :Event                                                              *
    *Entries :    10000 : BranchElement (see below)                              *
    *............................................................................*
    *Br    0 :fUniqueID : UInt_t                                                 *
    *Entries :    10000 : Total  Size=      40665 bytes  File Size  =        400 *
    *Baskets :        2 : Basket Size=      32000 bytes  Compression= 100.40     *
    *............................................................................*
    *Br    1 :fBits     : UInt_t                                                 *
    *Entries :    10000 : Total  Size=      80825 bytes  File Size  =      12895 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.23     *
    *............................................................................*
    *Br    2 :iEventNumber : Int_t                                               *
    *Entries :    10000 : Total  Size=      40683 bytes  File Size  =      14188 *
    *Baskets :        2 : Basket Size=      32000 bytes  Compression=   2.83     *
    *............................................................................*
    *Br    3 :iRunNumber : Int_t                                                 *
    *Entries :    10000 : Total  Size=      40671 bytes  File Size  =        431 *
    *Baskets :        2 : Basket Size=      32000 bytes  Compression=  93.18     *
    *............................................................................*
    *Br    4 :volumes   : Int_t volumes_                                         *
    *Entries :    10000 : Total  Size=      84634 bytes  File Size  =      20036 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   4.01     *
    *............................................................................*
    *Br    5 :volumes.sName : string sName[volumes_]                             *
    *Entries :    10000 : Total  Size=     490475 bytes  File Size  =      56539 *
    *Baskets :       17 : Basket Size=      32000 bytes  Compression=   8.66     *
    *............................................................................*
    *Br    6 :volumes.iVolumeID : Int_t iVolumeID[volumes_]                      *
    *Entries :    10000 : Total  Size=     121325 bytes  File Size  =      36256 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   3.33     *
    *............................................................................*
    *Br    7 :volumes.dTotalEnergyDep_keV :                                      *
    *         | Double_t dTotalEnergyDep_keV[volumes_]                           *
    *Entries :    10000 : Total  Size=     201769 bytes  File Size  =      17189 *
    *Baskets :        8 : Basket Size=      32000 bytes  Compression=  11.70     *
    *............................................................................*
    *Br    8 :volumes.iTotalOptPhotNumber : Int_t iTotalOptPhotNumber[volumes_]  *
    *Entries :    10000 : Total  Size=     121425 bytes  File Size  =      16757 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   7.21     *
    *............................................................................*
    *Br    9 :volumes.iTotalThermElecNumber :                                    *
    *         | Int_t iTotalThermElecNumber[volumes_]                            *
    *Entries :    10000 : Total  Size=     121445 bytes  File Size  =      16560 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   7.29     *
    *............................................................................*
    *Br   10 :primaryParticles : Int_t primaryParticles_                         *
    *Entries :    10000 : Total  Size=      86175 bytes  File Size  =      12915 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.22     *
    *............................................................................*
    *Br   11 :primaryParticles.sName : string sName[primaryParticles_]           *
    *Entries :    10000 : Total  Size=     241564 bytes  File Size  =      17254 *
    *Baskets :        9 : Basket Size=      32000 bytes  Compression=  13.96     *
    *............................................................................*
    *Br   12 :primaryParticles.dPosition_mm[3] :                                 *
    *         | Double_t dPosition_mm[primaryParticles_]                         *
    *Entries :    10000 : Total  Size=     281947 bytes  File Size  =     244734 *
    *Baskets :       11 : Basket Size=      32000 bytes  Compression=   1.15     *
    *............................................................................*
    *Br   13 :primaryParticles.dDirection[3] :                                   *
    *         | Double_t dDirection[primaryParticles_]                           *
    *Entries :    10000 : Total  Size=     281917 bytes  File Size  =     249817 *
    *Baskets :       11 : Basket Size=      32000 bytes  Compression=   1.13     *
    *............................................................................*
    *Br   14 :primaryParticles.dEnergy_keV :                                     *
    *         | Double_t dEnergy_keV[primaryParticles_]                          *
    *Entries :    10000 : Total  Size=     121321 bytes  File Size  =      86900 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   1.39     *
    *............................................................................*
    *Br   15 :primaryParticles.dTime_ns : Double_t dTime_ns[primaryParticles_]   *
    *Entries :    10000 : Total  Size=     121291 bytes  File Size  =      12604 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   9.57     *
    *............................................................................*
    *Br   16 :primaryParticles.sVolumeName :                                     *
    *         | string sVolumeName[primaryParticles_]                            *
    *Entries :    10000 : Total  Size=     301896 bytes  File Size  =      20004 *
    *Baskets :       11 : Basket Size=      32000 bytes  Compression=  15.06     *
    *............................................................................*
    *Br   17 :primaryParticles.iVolumeID : Int_t iVolumeID[primaryParticles_]    *
    *Entries :    10000 : Total  Size=      81091 bytes  File Size  =      12944 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.21     *
    *............................................................................*
    *Br   18 :tracks    : Int_t tracks_                                          *
    *Entries :    10000 : Total  Size=      87140 bytes  File Size  =      12875 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.24     *
    *............................................................................*
    *Br   19 :tracks.sParticleName : string sParticleName[tracks_]               *
    *Entries :    10000 : Total  Size=     241518 bytes  File Size  =      17233 *
    *Baskets :        9 : Basket Size=      32000 bytes  Compression=  13.98     *
    *............................................................................*
    *Br   20 :tracks.iParticleID : Int_t iParticleID[tracks_]                    *
    *Entries :    10000 : Total  Size=      81007 bytes  File Size  =      12899 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.23     *
    *............................................................................*
    *Br   21 :tracks.iTrackID : Int_t iTrackID[tracks_]                          *
    *Entries :    10000 : Total  Size=      80983 bytes  File Size  =      12910 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.23     *
    *............................................................................*
    *Br   22 :tracks.iParentID : Int_t iParentID[tracks_]                        *
    *Entries :    10000 : Total  Size=      80991 bytes  File Size  =      12889 *
    *Baskets :        4 : Basket Size=      32000 bytes  Compression=   6.24     *
    *............................................................................*
    *Br   23 :tracks.sCreatorProcess : string sCreatorProcess[tracks_]           *
    *Entries :    10000 : Total  Size=     181342 bytes  File Size  =      16764 *
    *Baskets :        7 : Basket Size=      32000 bytes  Compression=  10.78     *
    *............................................................................*
    *Br   24 :tracks.steps : vector<stepInfo> steps[tracks_]                     *
    *Entries :    10000 : Total  Size=    1818551 bytes  File Size  =     833809 *
    *Baskets :       59 : Basket Size=      32000 bytes  Compression=   2.18     *
    *............................................................................*
    *Br   25 :tracks.dWavelength_nm : Double_t dWavelength_nm[tracks_]           *
    *Entries :    10000 : Total  Size=     121231 bytes  File Size  =      89534 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   1.35     *
    *............................................................................*
    *Br   26 :tracks.dCharge : Double_t dCharge[tracks_]                         *
    *Entries :    10000 : Total  Size=     121161 bytes  File Size  =      12633 *
    *Baskets :        6 : Basket Size=      32000 bytes  Compression=   9.54     *
    *............................................................................*


As we can see there are a lot of information in the file.
It's broken down into two levels;

1. primaries
    * what was initially put in
2. tracks
    * the information of the particle propagation
    * what happened in each step of the event

To understand these better, you should download the file and explore it in the TBrowser

.. todo:: Add plotting of simple variables


Now lets try something a bit more complicated.
Lets create a ROOT histogram which contains number of photons which hit PMTs.
Look in the Solutions directory for help on this.