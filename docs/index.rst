.. include:: ../README.rst
   :end-before: Full Tutorial

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   coding_basics
   lz_overview
   nersc_running
   greatlakes_running
   greatlakes_batchjobs
   greatlakes_data
   baccarat_tutorial

.. toctree::
   :maxdepth: 1
   :caption: Code reference
   :glob:

   api/*
