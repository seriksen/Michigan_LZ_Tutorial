The Basics
==========

In order to follow this tutorial it is useful to cover some coding basics.
This tutorial was written using a Linux machine, however - you can also run it on Windows and Mac - although some
tweaks may be necessary.
The reason for this being written on Linux that both the Great Lakes and NERSC use the CentOS7 Operating System - so
knowing how to interact with it is very essential in order to run any LZ software.

Bash
----
The primary way of interacting with a cluster machine (such as NERSC or GreatLakes) is via text commands - connecting
to them via ssh (more on this later).
On Windows you can use the `command prompt`, and on Mac and Linux - the `terminal`.

`Bash <https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29>`_ is the interface that you interact with when on a
terminal (text window). An example is shown in the image below.

.. image:: images/bash.png

In the above example, several important bash commands are used.

1. :code:`pwd`: print working directory - Get the current folder you are in
2. :code:`mkdir`: Make a new directory.
3. :code:`cd`: change directory. Here we are moving into the directory we just created.
4. :code:`ls`: list files. Here we can see that no files exist in the directory we just created.

Try opening up a terminal and creating the directory. Play around with navigating.

Text Editing
~~~~~~~~~~~~
In the terminal you do not have a graphic user interface, however, that does not mean you cannot edit files
(such as code you're testing).
There are several text editors installed on most machines (listed below).
They all have similar functionality, and are very different from any other text editor you will have used before (such
as Word).

1. :code:`vi` or :code:`vim`; `vi tutorial <https://www.ks.uiuc.edu/Training/Tutorials/Reference/virefcard.pdf>`_
2. :code:`emacs`; `emacs tutorial <https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf>`_
3. :code:`nano`; `nano tutorial <https://www.lifewire.com/beginners-guide-to-nano-editor-3859002>`_

They all have their pros and cons, and people are very defensive about the one they pick...
It doesn't matter what you choose, as long as it works for you and you learn how to use it efficiently.

If you need help deciding, `here <https://unix.stackexchange.com/questions/986/what-are-the-pros-and-cons-of-vim-and-emacs>`_
are the pros and cons of :code:`vim` and :code:`emacs`

A good task, which will help you learn more :code:`bash` commands is to write a small bash script which creates a
directory, creates an empty file (you can use :code:`touch`) and outputs a message to the screen (use :code:`echo`).

For more advanced usage, look up :code:`alias`, and try add some to your :code:`.bashrc` file to make some commands
easier (you'll probably want these later then logging onto remote machines)

.. note::
   In order to use Bash on Windows, you need to enable the
   `Windows Subsystem for Linux <https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/>`_
   The easiest thing is just to download the ubuntu cmd from the windows store I believe? Not tested yet.

SSH
~~~
Now that you can perform actions via the command line, we can log onto one of the machines.
How to do that for NERSC and the GreatLakes isn't explained here,
instead refer to :doc:`nersc_running` or :doc:`greatlakes_running`.
Instead, this section explains how the basics of the connection.
There are many more detailed explanations of `ssh <https://en.wikipedia.org/wiki/SSH_%28Secure_Shell%29>`_.

:code:`ssh` can be boiled down to essentially a way of logging into a machine remotely via the command-line.
It is how we connect to both NERSC and GreatLakes.
It is another :code:`bash` command. You can find more details of by typing the following into the terminal
:code:`ssh -h` or :code:`man ssh`.

When using :code:`ssh`, it typically takes the following form;

.. code-block:: sh

   ssh [OPTIONS] <username>@<address_of_machine>

In the above case; :code:`username` is your login username to the remote machine, and :code:`address_of_machine` is the
address used to access the remote machine.

A typical OPTION you may want is one or more of the following; :code:`-A -Y`. What these actually do is shown in the
ssh manual.


Getting code
~~~~~~~~~~~~
Code is typically stored in the cloud (similar to OneDrive, Google Drive, etc...) but is slightly different.
The main providers are `gitlab <https://gitlab.com/>`_, `github <https://github.com/>`_ and
`bitbucket <https://bitbucket.org/product/>`_.
Note that they have `git` in their names. This is because most version control for code is handled by a system call
`git`.

One thing that we can do with git is download code from one site.
You will need to download this code onto the remote machine you are working on in order to follow the rest of this
tutorial.
For example, we can download this code by the following;

.. code-block:: sh

    git clone git@gitlab.com:seriksen/Michigan_LZ_Tutorial.git

It is worth learning how to use git as it's used extensively and you will want to be saving your code quite often.
You should also look up ssh keys for github and gitlab.

Python
------
This tutorial uses bash and python.
For python, I have written this so you can use a `conda environment <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_
It is recommended that you follow the NERSC conda guide or Great Lakes conda guide,
but for reference, here are the basic instructions;

.. code-block:: sh

    # Go to HOME directory
    cd
    # Get miniconda
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    # Run install script
    bash Miniconda3-latest-Linux-x86_64.sh

Install miniconda in your :code:`$HOME`.

When you :code:`git clone` this, you will see a file called `environment.yml`, you can use this to create a conda
environment by;

.. code-block:: sh

   conda env create -f environment.yml

You will then have a conda environment called :code:`michigan_tutorial`.

You may want to add something into your :code:`.bashrc` file so that :code:`conda` is sourced every time you open a
new terminal. If :code:`conda` has been installed in :code:`${HOME}/miniconda3` then you can add something like
:code:`source ${HOME}/miniconda3/etc/profile.d/conda.sh` to the end of your :code:`${HOME}/.bashrc` file. You should
look up what :code:`.bashrc` is before you start editing it though.