Running at GreatLakes
=====================

To get setup on the Great Lakes;

1. `Request an account <https://arc-ts.umich.edu/login-request>`_
2. Ask for access to the LZ account (email or slack Sam Eriksen - access takes a few days)

The great lakes has a lot of workshops and tutorial documentation on how to use their system so only a summary is
provided below.

Below are the instructions for running jobs on the login nodes.
See `Great Lakes Job Submission` for details on using compute nodes.

To login
~~~~~~~~

.. code-block::

   ssh <username>@greatlakes.arc-ts.umich.edu


.. include:: ../GreatLakes/Simulations/README.rst


.. include:: ../GreatLakes/ALPACA/README.rst


