Great Lakes Data
================

Data
----
Below is a list of data that is already available;

.. csv-table:: Great Lakes Data
   :file: ../GreatLakes/Data/gl_data.csv
   :widths: 70, 70, 10, 10, 50
   :header-rows: 1


.. include:: ../GreatLakes/Data/README.rst
