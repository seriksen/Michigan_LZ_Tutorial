Running at NERSC
================

.. note::
    If this is your first time trying to run. Run the tutorial in a jupyter notebook.

To get set up on cori;

1. Sign up for an account following the instructions on the `LZ primer slides <https://docs.google.com/presentation/d/1DOgM2hlBaTrMgyxoRPcqRaVp7wXi1FdD9YhTDxk9Cmo/edit>`_
2. Try and login: :code:`ssh <username>@cori.nersc.gov`
3. Consider setting up a proxy so you don't have to type in a password and OTP every time; `NERSC ssh proxy <https://docs.nersc.gov/connect/mfa>`_

At NERSC, all LZ software is run inside a shifter images.
More information about this can be found `here <https://luxzeplin.gitlab.io/docs/softwaredocs/computing/usdc/gettingStarted.html>`_
Before running the tutorial, enter the LZ container

.. code-block:: sh

    shifter --module=cvmfs --image=luxzeplin/offline_hosted:centos7 /bin/bash

More information on getting started on cori can be found `here <https://luxzeplin.gitlab.io/docs/softwaredocs/computing/usdc/gettingStarted.html>`_

Accessing LZ software
~~~~~~~~~~~~~~~~~~~~~
The LZ software is hosted on a file-system call :code:`cvmfs`.
You can access the LZ software want by running a setup script.
These setup scripts are located in the directory of the LZ software you are trying to access.
For example, to access the :code:`BACCARAT version 6.1.6` you can type to following;

.. code-block:: sh

    source /cvmfs/lz.opensciencegrid.org/BACCARAT/release-6.1.6/x86_64-centos7-gcc8-opt/setup.sh

Note that in order to successfully run this, you will need to be inside the shifter image.

Now you will have access the :code:`BACCARATExecutable` and can move onto the :doc:`baccarat_tutorial`.

Jupyter
-------
You can run the BACCARAT tutorial on NERSC within jupyter-lab entirely.
This is good as it means that all of the bash and python code can be combined together.
The sections of the jupyter notebook which are bash commands (and therefore would normally be run directly on the
terminal) start with :code:`%%script env bash`. This tells the system to run them as bash, rather than python.
This is often the quickest way to get going as all the code is already written (both python and bash).
`JupyterLab <https://jupyter.nersc.gov/>`_

