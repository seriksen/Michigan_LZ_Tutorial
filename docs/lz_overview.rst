The LZ Software
===============
The LZ software can generally be broken down into three sections;

1. Simulation
2. Reconstruction
3. RQ Analysis

Full description of the LZ software, along will indepth user guides can be found in the
`LZ software docs <https://luxzeplin.gitlab.io/docs/softwaredocs/>`_.
This is only meant to give a basic introduction to get simple simulations running.
As such, only the slow simulation chain is mentioned here, along with the binary BACCARAT output.
This may be updated in the future if this tutorial needs to cover them instead.

Simulation
----------
Particle Physics
~~~~~~~~~~~~~~~~
Particle Physics simulations are performed by BACCARAT (Basically A Component-Centric Analog Response to AnyThing).
It contains 4 executables.
The first is :code:`BACCARATExecutable`.
This is given a macro file which contains information on what to include in the simulation.

The output of this is a binary file, which can be converted into a ROOT file by :code:`BaccRootConverter` or
:code:`BaccRootReader`.
Typically you would use :code:`BaccRootConverter`, however if you are doing python analysis on a BACCARAT output, you
should use :code:`BaccRootReader`.

Finally, this is fed into :code:`BaccMCTruth`.
This add S2 optical photon information and consolidated the simulation information.

Electronics
~~~~~~~~~~~
The electronics is simulated by the DER (Detector Electronic Response).
It converts photons hitting a PMT into an actual electronic response - which will be very similar to what LZ would
actually see.
The DER is given the :code:`BaccMCTruth` output.


Reconstruction
--------------
The PMT signals recorded by the detector (and by the DER) need to be reconstructed so that the physics of interactions
can be understood.
This is done with LZap - LZ Analysis Package.
It performs reconstruction, determining energy deposits, positions, etc...
It outputs a bunch of reduced quantities (RQs) such as pulse and event classifications.

RQ Analysis
-----------
These reduced quantities can then be explored to look at specific physics.
LZ currently has two software options for this

1. :code:`ALPACA` (C++ and ROOT)
2. :code:`FAST` (python)