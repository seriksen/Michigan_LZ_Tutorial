.. image:: ../docs/images/quick_logo_2.png
   :target: https://gitlab.com/seriksen/Michigan_LZ_Tutorial

Michigan LZ Tutorial
====================

Welcome to the University of Michigan LZ software tutorial.
If this is your first time trying to run the LZ software, please read the full tutorial.

This Tutorial is designed to provide a walk-through on the basics of running some of the LZ software on two systems;

1. `NERSC cori <https://docs.nersc.gov/systems/cori/>`_
2. `Michigan Great Lakes computing <https://arc-ts.umich.edu/greatlakes/>`_

To run this tutorial, you will need to setup a conda environment on your chosen system.
Details on how to do this are in the tutorial.

Full Tutorial
-------------

The tutorial can be found `here <https://seriksen.gitlab.io/Michigan_LZ_Tutorial/>`_.


Building Documentation
----------------------

You can build this documentation locally by; :code:`sphinx-build -b html docs public`



