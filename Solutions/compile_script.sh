#!/bin/bash
########################################################
#
# Title: compile_script.sh
#
# Description: Script for compiling the PhotonsPerPMT code. It pulls a version of BACCARAT and enters your conda
#              environment to compile the code. Conda environment is needed as ROOT is there.
#
# Author: Sam Eriksen
#
########################################################
#
# Get BACCARAT if it's not already there
[ ! -d ${PWD}/baccarat ] && git clone git@gitlab.com:luxzeplin/sim/baccarat.git
#
# Set some variables
ROOT_DIRECTORY=${HOME}/miniconda3/envs/ddc10_analysis/include/ROOT
OUT_NAME=photons_per_pmt
IN_NAME=PhotonsPerPMT.cc
BACC_DIR=${PWD}/baccarat
#
# Enter conda environment
conda activate michigan_tutoral
#
# Compile code
g++ -o ${OUT_NAME} ${IN_NAME} ${PWD}/BaccRootConverterEvent_dict.cxx \
-I ${BACC_DIR} -I ${ROOT_DIRECTORY} `root-config --cflags --libs`
