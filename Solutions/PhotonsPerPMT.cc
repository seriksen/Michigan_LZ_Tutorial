// Standard includes
#include <cstdio>
#include <cstdlib>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <vector>

// ROOT includes
#include <TAxis.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH2Poly.h>
#include <TKey.h>
#include <TLine.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TNtuple.h>
#include <TObject.h>
#include <TProfile.h>
#include <TString.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TTree.h>
#include <TVector3.h>
#include <TVectorD.h>

// BACCARAT includes
#include "LZ/include/LZDetectorPMTBankCoordinates.hh"
#include "tools/BaccRootConverterEvent.hh"

std::vector<int> getPMTIDs(std::vector<std::string>* a);

int main(int argc, char* argv[]){

  std::string outfileName = "";

  /************************
   * Check the input file *
   ************************/
  try{
    if(argc<2) throw "No filename specified";
    std::string fileName = std::string(argv[1]);
    outfileName = fileName;
    fileName = fileName.substr(fileName.length()-5,5);
    if(fileName!=".root") throw "Not a ROOT file";
    outfileName = outfileName.substr(0,outfileName.length()-5);
    outfileName += "_hits.root";
    TFile* infile = new TFile(argv[1]);
    if(infile->IsZombie()) throw "Error opening file";
    if(!infile->GetListOfKeys()->Contains("HeaderTree"))
      throw "File does not contain the HeaderTree";
    if(!infile->GetListOfKeys()->Contains("DataTree"))
      throw "File does not contain the DataTree";
  }
  catch(const char* msg){
    std::cerr << msg << " - PhotonsPerPMT will exit" << std::endl;
    return 1;
  }

  // Read input file
  TFile* infile = new TFile(argv[1],"read");
  TTree* headerTree = (TTree*)infile->Get("HeaderTree");

  /*********************
   * HitMap histograms *
   *********************/

  // Create outfile
  TFile* outfile = new TFile(outfileName.c_str(),"recreate");

  std::vector<std::string>* components = 0;
  headerTree->SetBranchAddress("componentLookupTable",&components);
  headerTree->GetEntry(0);

  /********************
   * Set up Histogram *
   ********************/
  std::vector<int> pmtIndex{getPMTIDs(components)};

  TH1I* pmt_hit_hist = new TH1I("pmt_hit_hist", "pmt_hit_hist",
                                540, 0., 540.);


  /************************
   * Loop over data entry *
   ************************/
  // Get dataTree
  TTree* dataTree = (TTree*)infile->Get("DataTree");
  BaccRootConverterEvent* event{0};
  dataTree->SetBranchAddress("Event",&event);

  // Variable
  int volumeID{0};
  int pmt{0};
  int updateFrequency{1000}; // for outputting to terminal

  // Loop over events
  for(int i = 0; i<dataTree->GetEntries(); ++i){
    if(i%updateFrequency==0) std::cout << "Entry " << i << " of " <<  dataTree->GetEntries() << std::endl;

    dataTree->GetEntry(i);

    // Loop over each event track
    for(int j = 0; j<event->tracks.size(); ++j){

      // Get volume ID of where the photon ended
      volumeID = event->tracks[j].steps.back().iVolumeID-1;

      pmt = pmtIndex.at(volumeID);

      // If the photon hits a PMT
      if(pmt>-1){

        // Populate the hit histogram
        pmt_hit_hist->Fill(pmt);

      } // volume check
    } // events
  } // entries


  /*****************************
   * Tidy up and write outputs *
   *****************************/
  infile->Close();
  delete infile;

  //Write out the maps to the outfile
  pmt_hit_hist->Write(pmt_hit_hist->GetName());
  delete pmt_hit_hist;

  outfile->Close();
  delete outfile;

  return 0;
}

std::vector<int> getPMTIDs(std::vector<std::string>* components) {

  std::vector<int> pmtIndex;
  int pmtID;
  TString pmtString;
  TString component;

  for(size_t i = 0; i<components->size(); ++i){
    component = components->at(i);
    if(component.Contains("PMT_Photocathode") && !component.Contains("Skin") && (component.Contains("Top") || component.Contains("Bottom"))){

      // Get pmt number
      pmtString = component(component.Length()-3,3);
      pmtID = pmtString.Atoi();
      pmtIndex.push_back(pmtID);
    }
    else{
      pmtIndex.push_back(-1);
    }
  }
  return pmtIndex;
}