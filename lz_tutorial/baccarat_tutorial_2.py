import ROOT
import matplotlib.pyplot as plt

ROOT.gStyle.SetPalette(ROOT.kRainBow)


def electrons_vs_photons(r_file):
    """
    Plot the number of electrons vs the number of optical photons

    :param r_file: path to ROOT file
    :type r_file: str
    :return:
    :rtype: None
    """
    t_file = ROOT.TFile(r_file)
    data_tree = t_file.Get('DataTree')

    optical = data_tree.FindBranch('Event.volumes.iTotalOptPhotNumber')
    therm = data_tree.FindBranch('Event.volumes.iTotalThermElecNumber')

    n_therm = []
    n_photon = []

    length = optical.GetEntryOffsetLen()
    for i in range(optical.GetEntries()):
        optical.GetEntry(i)
        therm.GetEntry(i)
        n_photon.append(optical.GetValue(i, length))
        n_therm.append(therm.GetValue(i, length))
    fig, ax = plt.subplots(1, 1, figsize=(20, 10))
    ax.scatter(n_photon, n_therm)
    ax.set_title('nPhotons vs nElectrons')
    ax.set_ylabel('electrons')
    ax.set_xlabel('photons')
    plt.show()
