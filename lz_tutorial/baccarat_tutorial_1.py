import ROOT


def view_baccarat_contents(r_file):
    """
    Print the branches and leaves in the DataTree branch of a ROOT file

    :param r_file: path to ROOT file
    :type r_file: str
    :return:
    :rtype: None
    """
    t_file = ROOT.TFile(r_file)
    t_file.ls()
    events = t_file.Get("DataTree")
    events.Print()


def simple_baccarat_canvas(r_file, the_branch, hists):
    """
    Plot a series of histograms from one root file and one branch.

    :param r_file: path to ROOT file
    :type r_file: str
    :param the_branch: branch to examine
    :type the_branch: str
    :param hists: histograms to plot
    :type hists: list
    :return: Canvas
    :rtype: ROOT.TCanvas
    """
    canvas = ROOT.TCanvas("c", "c", len(hists) * 500, 500)
    canvas.Divide(len(hists), 1)
    t_file = ROOT.TFile(r_file)
    branch = t_file.Get(the_branch)
    for i, hist in enumerate(hists):
        canvas.cd(i + 1)
        branch.Draw(hist)
    return canvas
